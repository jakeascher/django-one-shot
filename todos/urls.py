from django.urls import path
from . import views

urlpatterns = [
    path("", views.list_todo_lists, name="todo_list_list"),
    path("<int:id>/", views.show_todo_list, name="todo_list_detail"),
    path("create/", views.create_todo_list, name="todo_list_create"),
    path("<int:id>/edit/", views.edit_todo_list, name="todo_list_update"),
    path("<int:id>/delete/", views.delete_todo_list, name="todo_list_delete"),
    path("items/create/", views.create_todo_item, name="todo_item_create"),
    path("items/<int:id>/edit/", views.edit_todo_item, name="todo_item_update"),
]
